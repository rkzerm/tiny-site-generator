/*******************************************************************************
* Tiny Site Generator - Generate static HTML pages.
* Copyright © 2022 Jesse Smith (jess@tinyplace.net) - All Rights Reserved.
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <https://www.gnu.org/licenses/>.
*******************************************************************************/

///////////////////////////////////////////////////////////////////////////////
// utils.c
///////////////////////////////////////////////////////////////////////////////
// Utility methods supporting the Tiny Site Generator tool.  This library is
// dependent on the llist linked list library included in the project.  
//
// Methods include:
//
//     copyFile() - Copies the binary contents of a file.  Used for copying
//         site asset files.
//     fstrstr() - Like the standard strstr() method but for file streams.
//         This method is used by the parser when scanning templates and
//         content for directives.
//     startswith() - Determine if a string starts with a sub string.
//     endswith - Determine if a string ends with a sub string.
//     splitstr() - Split a string into sections based on a delimeter.
//     getFilesNamesByExtension() - Reads a directory for all files with a
//         given extension.  Used for finding HTML files.
//
// Copyright © 2022 Jesse Smith (jesse@tinyplace.net) - All Rights Reserved.

//////////////////////////////////////////////////////////////////////////////
// System Libraries

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <dirent.h>

///////////////////////////////////////////////////////////////////////////////
// Local Libraries

#include "./utils.h"

///////////////////////////////////////////////////////////////////////////////
// Copy a File
///////////////////////////////////////////////////////////////////////////////
// Create a copy of a file from a source path at a destination path.
//
// Inputs:
//     const char srcPath[] - Path to the source file to copy from.
//     const char dstPath[] - Path to the destination file to copy to.
//
// Returns: Integer indicating success of the copy operation.  0 for success,
//     -1 for failure.
// 
// Theory of Operation:
//
// Open two file streams in binary read and write mode respectively and copy
// bytes one at a time from source file to destination file until the end of
// file is reached.

int copyFile(const char srcPath[], const char dstPath[]) {
    FILE *srcPtr;   // Source file pointer.
    FILE *dstPtr;   // Destination file pointer.
    char ch;        // Buffer character.

    if ((srcPtr = fopen(srcPath, "rb")) == NULL) return -1; // Attempt to open source file for read and return -1 if failed.
    if ((dstPtr = fopen(dstPath, "wb")) == NULL) return -1; // Attempt to open destination file for write and return -1 if failed.

    while ((ch = fgetc(srcPtr)) != EOF) fputc(ch, dstPtr);  // Until we reach the end of file, copy bytes one at a time.

    fclose(srcPtr); // Close source file.
    fclose(dstPtr); // Close destination file.

    return 0;   // Return copy success status.
}

///////////////////////////////////////////////////////////////////////////////
// File String String
///////////////////////////////////////////////////////////////////////////////
// Like the standard library method strstr(), searches for the first occurence
// of a string within a file starting at the given file pointer and returns
// the offset in characters.  
//
// Inputs:
//     FILE *fptr - An open file pointer.
//     const char *str - String to search for within the file.
//
// Returns: Integer representing the offset of the first occurence of the given
//     string within the file.  Returns -1 if the end of the file was reached
//     and no match was found.
//
// Theory of Operation:
//
// Starting at the file pointer, read from the file one character at a time and
// check if the read character matches the first character of the search
// string.  If so, continue reading while iterating over the search string and
// checking each subsequent character.  Keep track of how far into the file
// we've read as this will be needed to calculate the offset of a match.  If we
// reach the end of the file before a match is found, as detechted by an EOF 
// character, then return -1 to indicate no match was found.  Otherwise, return
// the offset from the file pointer to the first occurence of the string.

int fstrstr(FILE *fptr, const char *str) {
    char ch;                    // Character buffer for reading from file.
    int offset = 0;             // Distance of string occurence from file pointer.
    int i = 0;                  // Index for iterating over the search string.

    int length = strlen(str);   // Store the length of the search string.

    while ((ch = fgetc(fptr)) != EOF) { // Read file one character at a time until the end of the file...
        if (ch == str[i]) { // If the read character matches the next character in the search string...
            if (++i == length) {    // If we reach the end of the search string...
                offset -= i - 1;    // Its a match!  Set the offset to the beginning of the string index and break.
                break;
            }
        }

        else {  // Else the read character does not match the next character in the search string...
            i = 0;  // Reset the search string index and keep looking.
        }

        offset++;   // Keep track of how far we've searched.
    }

    if (ch == EOF) offset = -1; // If we reached the end of the file, assume no match was found and return -1.

    return offset;  // Return distance from the file pointer to the first occurence of the string, or -1 for no match.
}

///////////////////////////////////////////////////////////////////////////////
// Starts With
///////////////////////////////////////////////////////////////////////////////
// Check to see if a string begins with a given substring.  Strings are
// expected to be null terminated.
//
// Inputs:
//     const char *str - The string to check.
//     const char *sub - The substring to look for.
//
// Returns: True if the string begins with the substring, otherwise, false.  
//     Also returns false if the length of either string is zero.
//
// Theory of Operation:
// 
// Iterates over the two strings in tandem, comparing characters from each to
// see they are equal.  If a mismatch occurs, break the loop which returns the
// initial default match state of false.  If the end of the substring is
// reached, set the match state to true and break the loop.  

bool startswith(const char *str, const char *sub) {
    int i = 0;                      // Index for iterating the string.
    int subLength = strlen(sub);    // Store length of the substring.
    bool match = false;             // Presume a false match state until proven true.

    if (subLength != 0) {   // If the length of the substring is not 0...
        while (str[i] != '\0') {    // So long as we haven't reached the end of the string...
            if (str[i] != sub[i]) break;    // If the next characters don't match, break.
            if (++i == subLength) { // If we are at the end of the substring after incrementing the index...
                match = true;   // It's a match!  Set match state to true and break.
                break;
            }
        }
    }

    return match;   // Return the match state.
}

///////////////////////////////////////////////////////////////////////////////
// Ends With
///////////////////////////////////////////////////////////////////////////////
// Check to see if a string ends with a given substring.  Strings are expected 
// to be null terminated.
//
// Inputs:
//     const char *str - The string to check.
//     const char *sub - The substring to look for.
//
// Returns: True if the string ends with the substring, otherwise, false.  Also
//     returns false if the substring length is zero or the substring is longer
//     than the string.
//
// Theory of Operation:
//
// Set a match state to false.  Check excluding conditions such as substring
// longer than string.  If it checks out, iterate through both strings starting
// from the last character of string and substring checking if they are equal.
// If an inequality is found, break the loop.  Otherwise, if we reach the end
// of the substring with no discrepancy, set the match state to true.  Return
// the match state.

bool endswith(const char *str, const char *sub) {
    int i;                          // String index.
    int strLength = strlen(str);    // Length of the string.
    int subLength = strlen(sub);    // Length of the substring.
    bool match = false;             // Match state defaulted to false.

    if (subLength != 0 && subLength <= strLength) { // If the substring is not empty and is not longer than the string...
        for (i = 0; i < subLength; i++) {   // For each character in the substring...
            if (str[strLength - i] != sub[subLength - i]) break;    // Work backwards and break if any characters are not equal.
        }

        if (i == subLength) match = true;   // If we reached the end and not discrepancy was found, it's a match.
    }

    return match;   // Return the match state.
}

///////////////////////////////////////////////////////////////////////////////
// Split String
///////////////////////////////////////////////////////////////////////////////
// Split a string into segments based on a delimeter.  
//
// Inputs:
//     const char *str - The string to be split.
//     const char *del - A delimeter string marking where the splits will
//         occur.
//
// Returns: Pointer to LinkedList of string segments.
//
// Theory of Operation:
//
// Use the strstr method to sequentially search for instances of the delimeter.
// Use the strstr offset to calculate the length of the segment and allocate
// memory for it.  Append these segments to a linked list.  


LinkedList *splitstr(const char *str, const char *del) {
    LinkedList *list = LL_New();    // Instantiate a linked list.
    char * a = str;                 // Marker for beginning of a delimeter.
    char * b = str;                 // Marker for beginning of a segment.
    int delimeterLength = strlen(del);  // Length of the delimeter.
    int i;                          // String index for copying.

    while ((a = strstr(b, del)) != NULL) {  // While there are more delimeters...
        char *buffer = (char *)malloc(sizeof(char) * ((a - b) + 1));    // Allocate space for the segment.
        
        for (i = 0; i < (a - b); i++) { // For each character in the segment...
            buffer[i] = b[i];   // Copy the characters into the buffer.
        }
        
        buffer[i] = '\0';   // Null terminate the segment string.
        b = a + delimeterLength;    // Advance the segment beginning pointer to the next segment.
        LL_Append(list, buffer);    // Append the segment to the list.
    }

    if (strlen(b) > 0) {    // If there is another segment after the last delimeter...
        char *buffer = (char *)malloc((sizeof(char)) * (strlen(b) + 1));    // Allocate memory for it.
        strcpy(buffer, b);  // Copy it.
        LL_Append(list, buffer);    // Append it.
    }

    return list;    // Return the list of segments.
}

///////////////////////////////////////////////////////////////////////////////
// Get File Names by Extension
///////////////////////////////////////////////////////////////////////////////
// Read the contents of a directory and build a list of all file names ending
// with a given extension.
//
// Inputs:
//     const char *dirPath - Path of the directory to search.
//     const char *ext - Extension string to filter by.
//
// Returns: Pointer to LinkedList of file name strings.  Returns an empty list
//     if there were no matches and NULL on a directory read error.
//
// Theory of Operation:
//
// Use the dirent library to read the entries of a directory.  Iterate through
// the directory entries, using the endswith() method to check the end of the
// filenames for the given extension.  Copy the matching file names, append
// them to a linked list, and return the list.

LinkedList *getFileNamesByExtension(const char *dirPath, const char *ext) {
    struct dirent *dirEntry;    // Directory entry pointer.
    LinkedList *list = NULL;    // LinkedList pointer to NULL by default.
    
    DIR *dirPtr = opendir(dirPath); // Attempt to open the directory.

    if (dirPtr != NULL) {   // If the attempt was successful...
        list = LL_New();    // Instantiate a new linked list.

        while ((dirEntry = readdir(dirPtr)) != NULL) {  // While there are still directory entries to read...
            if (endswith(dirEntry->d_name, ext)) {  // If the name ends with the right extension...
                char *fileName = (char *)malloc(sizeof(char) * (strlen(dirEntry->d_name) + 1)); // Allocate space for the name string.
                strcpy(fileName, dirEntry->d_name); // Copy the file name.
                LL_Append(list, fileName);  // Append the file name to the list.
            }
        }
    }
    
    return list;    // Return the list, or NULL on directory error.
}

// Copyright © 2022 Jesse Smith (jesse@tinyplace.net) - All Rights Reserved.
