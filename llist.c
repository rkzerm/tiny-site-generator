/*******************************************************************************
* Tiny Site Generator - Generate static HTML pages.
* Copyright © 2022 Jesse Smith (jess@tinyplace.net) - All Rights Reserved.
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <https://www.gnu.org/licenses/>.
*******************************************************************************/

///////////////////////////////////////////////////////////////////////////////
// llist.c
///////////////////////////////////////////////////////////////////////////////
// Implementation of a linked list structure along with supporting methods.  
// This linked list implementation is non-cyclic and bidirectional.  The
// implementation comprises two structures.  
//
// The first is the LinkedListNode which forms the elemental component of the
// list.  Each element of the list is a LinkedListNode.  The LinkedListNode
// structure maintains a pointer to the next and previous nodes in the list.
// It also maintains a pointer of type void which points to the item tracked by
// the list.
//
// The second is the LinkedList structure which maintains pointers to the first
// and last node of the list.  It also tracks the number of elements in the
// list.  The LinkedList structure is used as a list reader instance, holding
// a pointer to the current node of the list.
//
// Pointers will be NULL when not pointing to a vaild node.  The first node of
// a list will have a previous node pointer set to NULL as there is no previous
// node.  Likewise, the last node of the list will have a next pointer set to
// NULL as there is no node after the last one.  An empty list will have all
// its pointers set to NULL.  The current node pointer of a list can be NULL in
// the following situations:
//
//     * When the list is empty and there are no nodes to point to.
//     * After a delete operation on the last node of the list.
//
// Several operations modify the current node pointer (LinkedList.ptr).  Much
// like reading a file stream, a seek operation is provided which allows
// resetting the current node pointer to a known index when necessary.
//
// Common Examples:
//     
//     ////////////////////////////////////////////////////////////////////////
//     // Iterating a list.
//
//     void doForEach(LinkedList * list) {
//         void *item;  // Pointer to list item.
//
//         LL_Seek(list, 0);    // Set node pointer to the start of the list.
//         item = LL_Get(list); // Get the current (first) item.
//
//         while (item != NULL) {   // Until we reach the end of the list...
//             doSomething(item);   // Do something with this item.
//             item = LL_Next(list);    // Advance to the next node and return a pointer to the next item.
//         }
//     }
//
// Copyright © 2022 Jesse Smith (jesse@tinyplace.net) - All Rights Reserved.

///////////////////////////////////////////////////////////////////////////////
// System Libraries

#include <stdlib.h>
#include <stdbool.h>

///////////////////////////////////////////////////////////////////////////////
// Local Libraries

#include "./llist.h"

///////////////////////////////////////////////////////////////////////////////
// New List
///////////////////////////////////////////////////////////////////////////////
// Instantiate a new list.
//
// Inputs: void
//
// Returns: Pointer to an empty LinkedList structure.
//
// Theory of Operation:
//
// Allocate memory for the LinkedList structure and initialize node pointers to
// NULL and set length to zero.  Return a pointer to this new list.

LinkedList *LL_New() {
    LinkedList *list = (LinkedList *)malloc(sizeof(LinkedList));    // Allocate memory for list structure.

    // There are no nodes in the list so...
    list->root = NULL;  // Set the first node pointer to NULL.
    list->ptr = NULL;   // Set the current node pointer to NULL.
    list->last = NULL;  // Set the last node pointer to NULL.
    list->length = 0;   // Set the length of the list to zero.

    return list;    // Return pointer to the list.
}

///////////////////////////////////////////////////////////////////////////////
// Append List
///////////////////////////////////////////////////////////////////////////////
// Append an item to the end of the linked list.
//
// Inputs:
//     LinkedList *list - Pointer to list to be appended.
//     void *item - Pointer to an object to append to the list.
//
// Returns: void
//
// Theory of Operation:
//
// Allocate space for a new node that will track the appended item.  Link the
// new node with the list by setting the appropriate next and previous
// pointers.  Update the LinkedList structure root and last pointers
// appropriately.  Increment the length of the list.

void LL_Append(LinkedList *list, void *item) {
    LinkedListNode *node = (LinkedListNode *)malloc(sizeof(LinkedListNode));    // Allocate memory for a new node.
    
    node->item = item;  // The new node will reference the given item.
    node->next = NULL;  // It will be the last node so it has no next node.
    node->prev = list->last;    // It's previous node is the current last node in the list.
    
    if (list->last != NULL) list->last->next = node;    // If there is a last node already, set its next pointer to the new node.
    list->last = node;  // The last node of the list is now the new node.

    if (list->length == 0) {    // If we appended an empty list...
        list->root = node;  // The new node is now the root node of the list.
        list->ptr = node;   // Set the current node pointer to the new node.
    }

    list->length++; // Increment the length of the list.
}

///////////////////////////////////////////////////////////////////////////////
// Delete Node
///////////////////////////////////////////////////////////////////////////////
// Delete the current node of the list.  
//
// Inputs:
//     LinkedList *item - Pointer to a list to delete from.
//
// Returns: Pointer to the item reference by the deleted node.  Returns NULL if
//     the current node is NULL.
//
// Theory of Operation:
//
// If the current node isn't NULL, remove node neighbor references to this node
// and update the list pointers appropriately.  Store a reference to the item
// represented by the node and call free on the node object.  Return the item
// pointer.

void *LL_Delete(LinkedList *list) {
    LinkedListNode *node = list->ptr;   // Get the current node.
    void *item = NULL;                  // Default the item pointer to NULL.

    if (node != NULL) { // If the current node is not NULL...
        if (node->prev != NULL) node->prev->next = node->next;  // If this is not the root node, point its previous neighbor to its next neighbor.
        if (node->next != NULL) node->next->prev = node->prev;  // If this is not the last node, point its next negihbor to its previous neighbor.
        else list->last = node->prev;   // If it is the last node, update the last pointer of the list to the nodes previous negihbor.

        list->ptr = node->next; // Set the current node to the next node.
        list->length--; // Decrement the length of the list.
        item = node->item;  // Store a pointer to the item.
        free(node); // Free the node.
    }
    
    return item;    // Return the item pointer.
}

///////////////////////////////////////////////////////////////////////////////
// Destroy List
///////////////////////////////////////////////////////////////////////////////
// Destroy an entire list, freeing up the memory.  Optionally, this method can
// call free() on every item referenced by the list.
//
// Inputs:
//     LinkedList *list - Pointer to the list to destroy.
//     bool deep - Set true to call free() on all referenced items.
//
// Returns: void
//
// Theory of Operation:
//
// Seek to the beginning of the list.  Iterate through call delete on each node
// and optionally calling free() for each item referenced by the node.  
// Finally, call free() on the list structure.

void LL_Destroy(LinkedList *list, bool deep) {
    void *item; // Pointer to item referenced by a node.

    LL_Seek(list, 0);   // Seek to beginning of the list.
    
    while ((item = LL_Delete(list)) != NULL) {  // Delete nodes while there are still nodes to delete...
        if (deep) free(item);   // If the "deep" delete option is true, free the referenced item as well.
    }

    free(list); // Free the list structure.
}

///////////////////////////////////////////////////////////////////////////////
// Insert Item
///////////////////////////////////////////////////////////////////////////////
// Insert an item before the current node in the list.
//
// Inputs:
//     LinkedList *list - Pointer to the list.
//     void *item - Pointer to an item to insert.
//
// Returns: void
//
// Theory of Operation:
//
// Allocate space for a new node.  Set the node to reference the item.  Set the
// node pointers to reference its new neighbors and the neighbors to reference
// the new node.  Increment the list length.

void LL_Insert(LinkedList *list, void *item) {
   LinkedListNode *node = (LinkedListNode *)malloc(sizeof(LinkedListNode)); // Allocate space for the node.

   node->item = item;   // Store a reference to the item.
   node->next = list->ptr;  // Current node becomes new node's next neighbor.
   node->prev = list->ptr->prev;    // Current node's previous neighbor is now new node's previous neighbor.

   if (node->next != NULL) node->next->prev = node; // If there is a next node, new node is its previous neighbor.
   if (node->prev != NULL) node->prev->next = node; // If there is a previous node, new node is its next neighbor.

   list->length++;  // Increment the list length.
}

///////////////////////////////////////////////////////////////////////////////
// Seek List
///////////////////////////////////////////////////////////////////////////////
// Set the current node of the list to a specific position given as an integer
// distance from either end of the list.  The position can be positive or
// negative.  If positive, the position indicates an index from the beginning
// of the list.  If negative, the position indecates an index from the end of
// the list.
//
// Inputs:
//     LinkedList *list - Pointer to a list.
//     int pos - Position to seek.
//
// Returns: void
//
// Theory of Operation:
//
// If pos is positive, set the current node to the root of the list and
// iteratively call LL_Next() on the list to advance the current node pointer
// the appropriate number of elements.  If pos is negative, do it in reverse.
// Set the current node to the last node of the list and advance backward using
// LL_Prev().

void LL_Seek(LinkedList *list, int pos) {
    int i;  // Loop counter.

    if (pos >= 0) { // If the given position is positive...
        list->ptr = list->root; // Goto the beginning of the list.

        for (i = 0; i < pos; i++) { // Walk forward the prescribed number of times.
            LL_Next(list);  // Calling next advances the current node pointer forward.
        }
    }

    else {  // Else the given position is negative...
        list->ptr = list->last; // Goto the end of the list.

        for (i = -1; i > pos; i--) {    // Walk backward the prescribed number of times.
            LL_Prev(list);  // Calling prev advances the current node pointer backward.
        }
    }
}

///////////////////////////////////////////////////////////////////////////////
// Next Item
///////////////////////////////////////////////////////////////////////////////
// Advance the current node pointer and return the next item.
//
// Inputs:
//     LinkedList *list - Pointer to a list.
//
// Returns: Pointer to the referenced item or NULL if the next node is NULL.
//
// Theory of Operation:
//
// If the current node is not NULL and the next node is not NULL, set the
// current node pointer to the next node and return the item reference from
// that node.

void *LL_Next(LinkedList *list) {
    void *item = NULL;  // Reference to an item.

    if (list->ptr != NULL) {    // If the current node is not NULL...
        if (list->ptr->next != NULL) {  // If the next node is not NULL...
            list->ptr = list->ptr->next;    // Advance the current node pointer to the next node.
            item = list->ptr->item; // Get the item reference.
        }
    }

    return item;    // Return the item reference.
}

///////////////////////////////////////////////////////////////////////////////
// Previous Item
///////////////////////////////////////////////////////////////////////////////
// Advance the current node pointer backward and return the previous item.
// 
// Inputs:
//     LinkedList *list - Pointer to a list.
//
// Returns: Pointer to the referenced item or NULL if the previous node is
//     NULL.
//
// Theory of Operation:
//
// If the previous node is not NULL, set the current node pointer to the
// previous node and return a reference to the item at that node.


void *LL_Prev(LinkedList *list) {
    void *item = NULL;  // Reference to an item.

    if (list->ptr->prev != NULL) {  // If the previous node is not NULL...
        list->ptr = list->ptr->prev;    // Set the current node pointer to the previous node.
        item = list->ptr->item; // Get the item reference.
    }

    return item;    // Return the item reference.
}

///////////////////////////////////////////////////////////////////////////////
// Get Item
///////////////////////////////////////////////////////////////////////////////
// Get the item at the current node in the list.
//
// Inputs:
//     LinkedList *list - Pointer to the list.
//
// Returns: Pointer to the item at the current node or NULL if the node is
//     NULL.
//
// Theory of Operation:
//
// Check to see that the node is not NULL and return the item referenced by the
// current node.

void *LL_Get(LinkedList *list) {
    void *item = NULL;  // Reference to an item.

    if (list->ptr != NULL) item = list->ptr->item;  // If the current node is not NULL, get the item reference.

    return item;    // Return the item reference.
}

///////////////////////////////////////////////////////////////////////////////
// Set Item
///////////////////////////////////////////////////////////////////////////////
// Set the item at the current node in the list.
//
// Inputs:
//     LinkedList *list - Pointer to the list.
//     void *item - Pointer to the item to set at the current node.
//
// Returns: void
//
// Theory of Operation:
//
// Check to see the current node isn't NULL and set the item pointer to the
// given item.

void LL_Set(LinkedList *list, void *item) {
    if (list->ptr != NULL) list->ptr->item = item;  // If the current node is not NULL, set the item reference.
}

///////////////////////////////////////////////////////////////////////////////
// Insert List
///////////////////////////////////////////////////////////////////////////////
// Insert a linked list into another linked list.  This operation destroys any
// reference to the inserted list.  The inserted list nodes will be inserted
// before the current node of the receiving list.
//
// Inputs:
//     LinkedList *list - Pointer to the receiving list.
//     LinkedList *fragment - Pointer to the list to be inserted.
//
// Returns: void
//
// Theory of Operation:
//
// If the list is empty, then simply copy the node references from the fragment
// structure to the list structure.  If the fragment is empty, do nothing.
// Otherwise, sew in the first and last nodes of the fragment list at the
// current node position by updating the next and previous pointers of the
// neighboring nodes then increase the list length by the length of the
// fragment.  Finally, free the fragment list structure.  All references to the
// inserted nodes now belong to the receiving list.

void LL_InserList(LinkedList *list, LinkedList *fragment) {
    if (list->length == 0) {    // If the receiving list is empty...
        list->root = fragment->root;    // Copy the fragment list members to the receiving list.
        list->ptr = fragment->root;
        list->last = fragment->last;
        list->length = fragment->length;
    }

    else if (fragment->length > 0) {    // If the fragment is not empty...
        fragment->last->next = list->ptr;   // The current node goes after the fragment.
        fragment->root->prev = list->ptr->prev; // The previous node goes before the fragment.

        if (fragment->last->next != NULL) fragment->last->next->prev = fragment->last;  // Update the neighbors if they are not NULL.
        if (fragment->root->prev != NULL) fragment->root->prev->next = fragment->root;

        list->length += fragment->length;   // Add the fragment length to the receiving list length.
    }

    free(fragment); // Free the fragment list structure.
}

// Copyright © 2022 Jesse Smith (jesse@tinyplace.net) - All Rights Reserved.
