/*******************************************************************************
* Tiny Site Generator - Generate static HTML pages.
* Copyright © 2022 Jesse Smith (jess@tinyplace.net) - All Rights Reserved.
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <https://www.gnu.org/licenses/>.
*******************************************************************************/

///////////////////////////////////////////////////////////////////////////////
// Tiny Site Generator
///////////////////////////////////////////////////////////////////////////////
// 

// Copyright © 2022 Jesse Smith (jesse@tinyplace.net) - All Rights Reserved.

///////////////////////////////////////////////////////////////////////////////
// System Headers

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/stat.h>

///////////////////////////////////////////////////////////////////////////////
// Local Headers

#include "./tinysite.h"
#include "./utils.h"
//#include "./llist.h"

///////////////////////////////////////////////////////////////////////////////
// Global Constants

const char defaultThemeDir[] = "/usr/local/share/tinysg/themes/tsg-light";
const char defaultInputDir[] = "./src";
const char defaultOutputDir[] = "./dist";

const char helpText[] = 
"Usage: tinysg [OPTIONS] DIRECTORY\n\
Generate a static HTML site from a collection of articles and meta data.\n\
\n\
  -o OUTPUT  Output directory path (Default: \"./dist\")\n\
  -t THEME   Theme name or path to theme (Default: \"tsg-light\")\n\
\n\
  --help     Display this help text and exit\n\
  --version  Display version and licensing information and exit";

const char versionText[] =
"Tiny Site Generator v0.0.2\n\
Copyright (C) 2022 Jesse Smith (jesse@tinyplace.net) - All Rights Reserved.\n\
License GPLv3: GNU GPL version 3 <https://gnu.org/licenses/gpl.html>.\n\
This is free software and you are welcome to modify and redistribute it under\n\
the terms of the GPLv3 license.\n\
This program comes with ABSOLUTELY NO WARRANTY.";

///////////////////////////////////////////////////////////////////////////////
// Global Variables

// Working directories:
char *themeDir;
char *inputDir;
char *outputDir;

// Global configurations:
LinkedList *siteConfig;
LinkedList *siteContexts;

// Templates:
LinkedList *rootTemplate;
LinkedList *navTemplate;
LinkedList *navButtonTemplate;
LinkedList *navButtonSelectedTemplate;
LinkedList *indexerTemplate;
LinkedList *indexCardTemplate;
LinkedList *articleTemplate;

///////////////////////////////////////////////////////////////////////////////
// Deep Copy Directive List
///////////////////////////////////////////////////////////////////////////////
// Create a duplicate of a linked list of directives including copies of all
// directives in the list.
//
// Inputs:
//     LinkedList *list - Pointer to linked list of directives.
//
// Returns: Pointer to the duplicate linked list structure.
//
// Theory of Operation:
//
// Allocate a new LinkedList to copy to.  Beginning at the root of the target
// list, iterate over the directives and deep copy each one with
// TS_CloneDirective() and append it to the duplicate list.  Return a reference
// to the copy.

LinkedList *deepCopyDirectiveList(LinkedList *list) {
    TS_Directive *directive;    // Current directive.
    LinkedList *copy = LL_New();    // Allocate a new list to copy to.

    LL_Seek(list, 0);   // Start from the beginning of the target list.
    directive = (TS_Directive *)LL_Get(list);   // Get the next directive in the list

    while (directive != NULL) { // Until we reach the end of the list...
        LL_Append(copy, TS_CloneDirective(directive));  // Copy the directive and append it to the copy list.
        directive = LL_Next(list);  // Get the next directive.
    }

    return copy;    // Return a reference to the copy.
}

///////////////////////////////////////////////////////////////////////////////
// Expand Directive List
///////////////////////////////////////////////////////////////////////////////
// Replace all directives of a specified type with a new list of directives.
// This directive list expansion is primarily used for template injection.
//
// Inputs:
//     LinkedList *list - Pointer to linked list of directives to be expanded.
//     LinkedList *fragment - Pointer to linke list of directives to inject.
//     char *dir - String name of the directive type to replace.
//
// Returns: void
//
// Theory of Operation:
//
// Iterate over the target directive list searching for directives that match
// the given type.  When one is encountered, delete that directive in place,
// then begin copying and inserting directives from the fragment list until the
// entire fragment list is injected where the search directive used to be.
// Count the number of fragment directives injected and use this to seek the 
// target linked list pointer to the end of the injected segment.  Continue
// searching for the directive type and repeat this process for each directive
// found.

void expandDirectiveList(LinkedList *list, LinkedList *fragment, char *dir) {
    TS_Directive *directive;    // Current target list directive.
    TS_Directive *fDirective;   // Current fragment list directive.
    int index = 0;  // Index for tracking position within the target list.

    LL_Seek(list, 0);   // Start at the beginning.
    directive = (TS_Directive *)LL_Get(list);   // Get the next directive.

    while (directive != NULL) { // Until we reach the end of the target list.
        if (TS_DirectiveTypeIs(directive, dir)) {   // If this is the target directive...
            TS_DestroyDirective(LL_Delete(list));   // Delete it from the list.
            
            LL_Seek(fragment, 0);   // Start at the beginning of the fragment.
            fDirective = (TS_Directive *)LL_Get(fragment);  // Get the next fragment directive.

            while (fDirective != NULL) {    // Until we reach the end of the fragment list...
                LL_Insert(list, TS_CloneDirective(fDirective)); // Insert the fragment directive into the target list.
                fDirective = LL_Next(fragment); // Get the next fragment.
                index++;    // Count the number of fragement directives inserted.
            }
        }

        LL_Seek(list, index);   // Reset the target list pointer to pass any inserted directives.
        directive = LL_Next(list);  // Get the next directive.
        index++;    // Count the directives we've searched.
    }
}

///////////////////////////////////////////////////////////////////////////////
// Destroy Directive List
///////////////////////////////////////////////////////////////////////////////
// Deallocate a linked list of directives, including all data pointed to by the
// list.
//
// Inputs:
//     LinkedList *list - Pointer to a linked list of directives.
//
// Returns: void
//
// Theory of Operation:
//
// Iterate through a linked list of directives calling TS_DestroyDirective() on
// each directive to free the allocated memory.  Once all directive structures
// have been freed, free the LinkedList structure with LL_Destroy().
//
// NOTE: This process cannot be accomplished with LL_Destroy(list, true) which
// performs a simple free() call on all data pointed to by the list because the
// TS_Directive structure itself stores pointers to allocated memory which must
// be freed.  This is why it is necessary to call TS_DestroyDirective() for
// each directive in the list.

void destroyDirectiveList(LinkedList *list) {
    TS_Directive *directive;    // Current directive.

    LL_Seek(list, 0);   // Start at the beginning of the list.
    directive = (TS_Directive *)LL_Get(list);   // Get the next directive.

    while(directive != NULL) {  // Until we reach the end of the list...
        TS_DestroyDirective(directive); // Free all memory associated with the directive.
        directive = LL_Next(list);  // Get the next directive.
    }
    
    LL_Destroy(list, false);    // Free all memory associated with the list structure.
}

///////////////////////////////////////////////////////////////////////////////
// Concatenate Directive List
///////////////////////////////////////////////////////////////////////////////
// Append a list of directives to the end of a target list.  This operation
// performs a deep copy of all elements appended.
//
// Inputs:
//     LinkedList *list - Pointer to the target linked list of directives.
//     LinkedList *fragment - Pointer to a linked list of directives to be
//         appended.
//
// Returns: void
//
// Theory of Operation:
//
// Iterate through the fragment list, deep copy each directive and append it to
// the target list.

void concatenateDirectiveList(LinkedList *list, LinkedList *fragment) {
    TS_Directive *directive;    // Current directive.
    
    LL_Seek(fragment, 0);   // Start at the beginning of the fragment list.
    directive = LL_Get(fragment);   // Get the next directive.

    while (directive != NULL) { // Until we reach the end of the fragment list...
        LL_Append(list, TS_CloneDirective(directive));  // Copy the directive and append it to the target list.
        directive = LL_Next(fragment);  // Get the next directive.
    }
}

///////////////////////////////////////////////////////////////////////////////
// Prune Directive List
///////////////////////////////////////////////////////////////////////////////
// Delete all directives of the specified type from the target list.
//
// Inputs:
//     LinkedList *list - Pointer to the target linked list of directives.
//     char *dir - String type of directive to delete.
//
// Returns: void
//
// Theory of Operation:
//
// Iterate throught the target list of directives, searching for directives of
// the specified type.  Delete each of the found directives from the list and
// call TS_DestroyDirective() on the directive structure to free memory.

void pruneDirectiveList(LinkedList *list, char *dir) {
    TS_Directive *directive;    // Current directive.
    
    LL_Seek(list, 0);   // Start at the beginning of the list.
    directive = (TS_Directive *)LL_Get(list);   // Get the next directive.

    while (directive != NULL) { // Until we reach the end of the list...
        if (TS_DirectiveTypeIs(directive, dir)) {   // If the directive matches the specified type...
            LL_Delete(list);    // Remove this directive from the list.
            TS_DestroyDirective(directive); // Free memory allocated to the directive.
            directive = (TS_Directive *)LL_Get(list);   // Get the next directive.
        }

        else {  // Else the directive type doesn't match...
            directive = LL_Next(list);  // Get the next directive and move on.
        }
    }
}

///////////////////////////////////////////////////////////////////////////////
// Rip Directive List
///////////////////////////////////////////////////////////////////////////////
// Remove all directives of the specified type from a list and return them as a
// new list.
//
// Inputs:
//     LinkedList *list - Pointer to target linked list of directives.
//     char *dir - String type of directive to rip.
//
// Returns: Pointer to a linked list containing the ripped directives.
//
// Theory of Operation:
//
// Allocate a new linked list.  Iterate through the target list searching for
// directives of the specified type.  For each maching directive, delete it
// from the target list and append it to the new list.  Return a reference to
// the new list.

LinkedList *ripDirectiveList(LinkedList *list, char *dir) {
    TS_Directive *directive;    // Current directive.
    LinkedList *rippedList = LL_New();  // Allocate a new list.
    
    LL_Seek(list, 0);   // Start from the beginning of the target list.
    directive = (TS_Directive *)LL_Get(list);   // Get the next directive.

    while (directive != NULL) { // Until we reach the end of the list...
        if (TS_DirectiveTypeIs(directive, dir)) {   // If the directive matches the specified type...
            LL_Delete(list);    // Delete the directive from the list.
            LL_Append(rippedList, directive);   // Append the directive to the new list.
            directive = (TS_Directive *)LL_Get(list);   // Get the next directive in the target list.
        }

        else {  // Else the directive type doesn't match...
            directive = LL_Next(list);  // Get the next directive and move on.
        }
    }

    return rippedList;  // Return a reference to the ripped list.
}

///////////////////////////////////////////////////////////////////////////////
// Write Output
///////////////////////////////////////////////////////////////////////////////
// Sequentially write the values of all STRING directives in a list to a file.
//
// Inputs:
//     char *filePath - Path to the output file.
//     LinkedList *list - Pointer to a linked list of directives.
//
// Returns: void
//
// Theory of Operation:
//
// STRING directives denote literal strings in the parsed document structure.
// Open a file for write.  Iterate through the list of directives searching for
// STRING directives.  For each one, write its first argument (a string) to the
// output file.  Close the file.

void writeOutput(char *filePath, LinkedList *list) {
    TS_Directive *directive;    // Current directive.
    FILE *fptr; // Output file pointer.

    fptr = fopen(filePath, "w");    // Open a file for write.

    LL_Seek(list, 0);   // Start at the beginning of the list.
    directive = (TS_Directive *)LL_Get(list);   // Get the next directive.

    while (directive != NULL) { // Until we reach the end of the list...
        if (TS_DirectiveTypeIs(directive, "STRING")) {  // If this is a STRING directive...
            fputs(directive->args[0], fptr);    // Copy the first argument (a literal string) to the file.
        }

        directive = (TS_Directive *)LL_Next(list);  // Get the next directive.
    }

    fclose(fptr);   // Close the file.
}

///////////////////////////////////////////////////////////////////////////////
// Sum Directive Arguments Length
///////////////////////////////////////////////////////////////////////////////
// Add up the length of all string arguments for a given directive.
//
// Inputs:
//     TS_Directive *directive - Pointer to a directive.
//
// Returns: Integer combined length of string arguments.
//
// Theory of Operation:
//
// The directive structure stores the number of arguments to the directive.
// Use this number to iterate over the arguments and sum their lengths
// calculated by strlen().  Return the combined length of all string arguments.

int sumDirectiveArgsLength(TS_Directive *directive) {
    int length = 0; // Total length accumluator.
    int i;  // Argument indexer.
    
    for (i = 0; i < directive->numArgs; i++) {  // For each argument of the directive...
        length += strlen(directive->args[i]);   // Calculate its length and add to the accumulator.
    }

    return length;  // Return the accumulated total.
}

///////////////////////////////////////////////////////////////////////////////
// Process Context
///////////////////////////////////////////////////////////////////////////////
// Given a directive list from a parsed document, perform the substitutions and
// translations based on a context.
//
// Inputs:
//     LinkedList *list - Pointer to linked list of directives from a parsed
//         file.
//     LinkedList *context - Pointer to a linked list of directives providing
//         values for substitution.
//
// Returns: void
//
// Theory of Operation:
//
// Input files processed by TSG often contain incomplete directives that are
// placeholders for values to be inserted based on the context of the
// processing.  Processed directives are converted into STRING type directives
// in preparation to write the output file.  To process the directive list 
// against the context, iterate over the directive list.  For each directive,
// check the context for a matching directive to substitute.  Perform any 
// special interpretation / sub-parsing of values based on directive type and
// build a STRING directive.  Delete the place holder directive from the list
// and insert the STRING directive in its place.  Repeat until all directives
// have been checked for context translations.

void processContext(LinkedList *list, LinkedList *context) {
    TS_Directive *directive;    // Current target directive.
    TS_Directive *contextDirective; // Current context directive.
    TS_Directive *subDirective; // Pointer to the substitution directive.

    LL_Seek(list, 0);   // Start from the beginning of the target list.
    directive = (TS_Directive *)LL_Get(list);   // Get the next directive.

    while (directive != NULL) { // Until we reach the end of the list...
        LL_Seek(context, 0);    // Start from the beginning of the context list.
        contextDirective = (TS_Directive *)LL_Get(context); // Get the next context directive.

        while (contextDirective != NULL) {  // Until we reach the end of the context list...
            if (directive->type == contextDirective->type) {    // If the context directive type matches the target directive...
                char **subArgs = (char **)malloc(sizeof(char *));   // Allocate space for the substitute directive arguments.
                
                if (TS_DirectiveTypeIs(contextDirective, "ARTICLEPUBLISHDATE")) {   // If it's an ARTICLEPUBLISHDATE directive...
                    subArgs[0] = (char *)malloc(sizeof(char) * (sumDirectiveArgsLength(contextDirective) + 3)); // Allocate space for the date string.
                    strcpy(subArgs[0], contextDirective->args[0]);  // Build the date string...
                    strcat(subArgs[0], "/");
                    strcat(subArgs[0], contextDirective->args[1]);
                    strcat(subArgs[0], "/");
                    strcat(subArgs[0], contextDirective->args[2]);
                }

                else {  // Else if its any other directive...
                    subArgs[0] = (char *)malloc(sizeof(char) * (strlen(contextDirective->args[0]) + 1));    // Allocate space to copy the first argument string.
                    strcpy(subArgs[0], contextDirective->args[0]);  // Copy the first argument string.
                }

                subDirective = TS_NewDirective("STRING", 1, subArgs);   // Create a new STRING directive with the substitute arguemnt.

                LL_Delete(list);    // Delete the target directive.
                LL_Insert(list, subDirective);  // Insert the substitute directive in its place.
            }

            contextDirective = (TS_Directive *)LL_Next(context);    // Get the next context directive.
        }

        directive = (TS_Directive *)LL_Next(list);  // Get the next target directive.
    }
}

///////////////////////////////////////////////////////////////////////////////
// Print Directive List
///////////////////////////////////////////////////////////////////////////////
// Print a representation of each directive to STDOUT.
//
// Inputs:
//     LinkedList *list - Pointer to a linked list of directives.
//
// Returns: void
//
// Theory of Operation:
//
// Iterate through a linked list of directives.  For each, print a string
// representation of its type, then iterate through its arguments and print
// each one.

void printDirectiveList(LinkedList *list) {
    int i;  // Argument indexer.
    TS_Directive *directive;    // Current directive.

    LL_Seek(list, 0);   // Start at the beginning of the list.
    directive = (TS_Directive *)LL_Get(list);   // Get the next directive.

    while (directive != NULL) { // Until we reach the end of the list...
        printf("%s:\n", TS_DirectiveGetType(directive));    // Print the directive type.
        for (i = 0; i < directive->numArgs; i++) printf("  |%s\n", directive->args[i]); // Print the directive arguments.
        
        directive = (TS_Directive *)LL_Next(list);  // Get the next directive.
    }
}

///////////////////////////////////////////////////////////////////////////////
// Build Nav
///////////////////////////////////////////////////////////////////////////////
// Build a navigation bar with the selected navigation label from a collection
// of templates.
//
// Inputs:
//     char *active - String name of the active navigation context.
//
// Returns: Pointer to linked list of directives representing the navigation
//     bar.
//
// Theory of Operation:
//
// The navigation bar is built through a series of template injections and
// substitution.  Navigation contexts are taken from the global site contexts.
// First, instantiate a new directive list for building the list of nav
// buttons.  Iterate over the site contexts building a button for each site
// context.  Building the button from the navButtonTemplate requires a
// directive context defining NAVURL and NAVLABEL.  This context is built from
// the site context directive.  A button template is chosen based on whether
// the current context is selected.  The chosen button template is processed
// against the button context to create the directive list.  This list is
// appended to the button list.  Once the button list is complete, we copy the
// nav template and expand the NAVBUTTONS place holder directive with the
// button list.  Return a reference to the completed nav bar template.

LinkedList *buildNav(char *active) {
    LinkedList *tempButtonList = LL_New();  // Instantiate a list for the buttons.
    TS_Directive *directive;    // Current directive index for siteContexts.

    LL_Seek(siteContexts, 0);   // Start at the beginning of the list.
    directive = (TS_Directive *)LL_Get(siteContexts);   // Get the next context directive.

    while(directive != NULL) {  // Until we reach the end of the site context list...
        LinkedList *context = LL_New(); // Instantiate a new list for building the button context.
        LinkedList *tempButton = LL_New();  // Instantiate a new list for building the button template.
        TS_Directive *buttonDirective;  // Indexer for the button directive list.
        
        // Build the NAVURL context directive...
        char **navURLargs = (char **)malloc(sizeof(char *));    // Allocate directive args.
        navURLargs[0] = (char *)malloc(sizeof(char) * (strlen(directive->args[1]) + 2));    // URL is the length of the site context path + 2 for the leading '/' and null terminator.
        strcpy(navURLargs[0], "/"); // Copy the leading '/' of the URL.
        strcat(navURLargs[0], directive->args[1]);  // Copy the site context path.
        LL_Append(context, TS_NewDirective("NAVURL", 1, navURLargs));   // Create a new NAVURL directive from the arguments and append it to the button context.

        // Build the NAVLABEL context directive...
        char **navLabel = (char **)malloc(sizeof(char *));  // Allocate directive args.
        navLabel[0] = (char *)malloc(sizeof(char) * (strlen(directive->args[0]) + 1));  // Button label is length of the site context label + 1 for the null terminator.
        strcpy(navLabel[0], directive->args[0]);    // Copy the site context label.
        LL_Append(context, TS_NewDirective("NAVLABEL", 1, navLabel));   // Create a new NAVLABEL directive from the arguments and append it to the button context.
      
        if (strcmp(directive->args[0], active) == 0) {  // If the current site context is the active context...
            tempButton = deepCopyDirectiveList(navButtonSelectedTemplate);  // Copy the selected button template.
        }

        else {  // Else this is not the active context...
            tempButton = deepCopyDirectiveList(navButtonTemplate);  // Copy the unselected button template.
        }

        processContext(tempButton, context);    // Process the button template against the button context.
        destroyDirectiveList(context);  // Destroy the button context as it is no longer needed.

        LL_Seek(tempButton, 0); // Start from the beginning of the button directive list.
        buttonDirective = LL_Get(tempButton);   // Get the next button directive.

        while (buttonDirective != NULL) {   // Until we reach the end of the list...
            LL_Append(tempButtonList, buttonDirective); // Add the directive to the main button list.
            buttonDirective = LL_Next(tempButton);  // Get the next directive.
        }

        LL_Destroy(tempButton, false);  // Destroy the directive list for this button after copy.
        directive = (TS_Directive *)LL_Next(siteContexts);  // Get the next site context directive.
    }

    LinkedList *tempNav = LL_New(); // Instantiate a new list for the nav template copy.
    tempNav = deepCopyDirectiveList(navTemplate);   // Make a copy of the nave template.
    expandDirectiveList(tempNav, tempButtonList, "NAVBUTTONS"); // Expand the NAVBUTTONS placeholder directive with the button list.
    destroyDirectiveList(tempButtonList);   // Destroy the button list.

    return tempNav; // Return a reference to the expanded nav list.
}

///////////////////////////////////////////////////////////////////////////////
// Build Index Card
///////////////////////////////////////////////////////////////////////////////
// Build an index card directive list from a global template and a content
// file.
//
// Inputs:
//     char *contentFile - String path to a content file.
//     char *url - String URL for the article reference by the card.
//
// Returns: Pointer to a linked list of directives representing an index card.
//
// Theory of Operation:
//
// 

LinkedList *buildIndexCard(char *contentFile, char *url) {
    LinkedList *context = TS_ParseFile(contentFile);
    LinkedList *content = ripDirectiveList(context, "STRING");
    LinkedList *tempIndexCard = deepCopyDirectiveList(indexCardTemplate);
    TS_Directive *articleURL;

    char **articleURLArgs = (char **)malloc(sizeof(char *));
    articleURLArgs[0] = (char *)malloc(sizeof(char) * (strlen(url) + 1));
    strcpy(articleURLArgs[0], url);
    articleURL = TS_NewDirective("ARTICLEURL", 1, articleURLArgs);
    LL_Append(context, articleURL);

    processContext(tempIndexCard, context);
    
    destroyDirectiveList(context);
    destroyDirectiveList(content);

    return tempIndexCard;
}

LinkedList *buildIndexer(LinkedList *indexCards) {
    LinkedList *tempIndexer = deepCopyDirectiveList(indexerTemplate);
    
    expandDirectiveList(tempIndexer, indexCards, "INDEXER");
    
    return tempIndexer;
}

LinkedList *buildArticle(char *contentFile, char *nextURL, char *prevURL) {
    LinkedList *context = TS_ParseFile(contentFile);
    LinkedList *content = ripDirectiveList(context, "STRING");
    LinkedList *tempArticle = deepCopyDirectiveList(articleTemplate);

    char **articlePrevURLArgs = (char **)malloc(sizeof(char *));
    articlePrevURLArgs[0] = (char *)malloc(sizeof(char) * (strlen(prevURL) + 1));
    strcpy(articlePrevURLArgs[0], prevURL);

    char **articleNextURLArgs = (char **)malloc(sizeof(char *));
    articleNextURLArgs[0] = (char *)malloc(sizeof(char) * (strlen(nextURL) + 1));
    strcpy(articleNextURLArgs[0], nextURL);

    LL_Append(context, TS_NewDirective("ARTICLEPREVURL", 1, articlePrevURLArgs));
    LL_Append(context, TS_NewDirective("ARTICLENEXTURL", 1, articleNextURLArgs));

    processContext(tempArticle, context);
    expandDirectiveList(tempArticle, content, "ARTICLECONTENT");

    destroyDirectiveList(context);
    destroyDirectiveList(content);

    return tempArticle;
}

LinkedList *buildPage(LinkedList *content, char *navContext) {
    LinkedList *tempRootTemplate;
    LinkedList *tempSiteConfig;
    LinkedList *tempNav;

    tempRootTemplate = deepCopyDirectiveList(rootTemplate);
    tempSiteConfig = deepCopyDirectiveList(siteConfig);
    expandDirectiveList(tempRootTemplate, content, "CONTENT");

    tempNav = buildNav(navContext);
    expandDirectiveList(tempRootTemplate, tempNav, "NAV");
    
    processContext(tempRootTemplate, tempSiteConfig);
    
    destroyDirectiveList(tempSiteConfig);
    destroyDirectiveList(tempNav);
    
    return tempRootTemplate;
}

LinkedList *buildArticleList(LinkedList *contentFileList, char *dir) {
    typedef struct {
        char *filename;
        char dateStr[9];
    } ArticleMetadata;

    char filePath[256];
    char *filename;
    LinkedList *content;
    LinkedList *temp;
    LinkedList *newList;
    int numArticles = 0;
    TS_Directive *directive;

    ArticleMetadata **articleMetas = malloc(sizeof(ArticleMetadata *) * contentFileList->length);

    LL_Seek(contentFileList, 0);
    filename = (char *)LL_Get(contentFileList);

    // Extract article metadata
    while (filename != NULL) {
        strcpy(filePath, dir);
        strcat(filePath, "/");
        strcat(filePath, filename);

        content = TS_ParseFile(filePath);
        temp = ripDirectiveList(content, "ARTICLEPUBLISH");
        directive = LL_Get(temp);
        
        if (strcmp(directive->args[0], "true") == 0) {
            destroyDirectiveList(temp);

            articleMetas[numArticles] = malloc(sizeof(ArticleMetadata));
            articleMetas[numArticles]->filename = malloc(sizeof(char) * (strlen(filename) + 1));
            strcpy(articleMetas[numArticles]->filename, filename);

            temp = ripDirectiveList(content, "ARTICLEPUBLISHDATE");
            directive = LL_Get(temp);
            strcpy(articleMetas[numArticles]->dateStr, directive->args[2]);
            strcat(articleMetas[numArticles]->dateStr, directive->args[0]);
            strcat(articleMetas[numArticles]->dateStr, directive->args[1]);

            destroyDirectiveList(temp);
            numArticles++;
        }

        filename = (char *)LL_Next(contentFileList);
    }
    
    // Bubble sort articles by date.
    if (numArticles > 1) {
        for (int i = 1; i < numArticles; i++) {
            int j = i;

            while (strcmp(articleMetas[j]->dateStr, articleMetas[j - 1]->dateStr) > 0) {
                printf("(%i) %s > %s\n", j, articleMetas[j]->dateStr, articleMetas[j - 1]->dateStr);

                ArticleMetadata *meta = articleMetas[j];
                articleMetas[j] = articleMetas[j - 1];
                articleMetas[j - 1] = meta;
                j--;

                if (j == 0) break;
            }
        }
    }

    // Build new filename list and cleanup.
    newList = LL_New();

    for (int i = 0; i < numArticles; i++) {
        LL_Append(newList, articleMetas[i]->filename);
        free(articleMetas[i]);
    }

    free(articleMetas);
    
    LL_Destroy(contentFileList, true);

    return newList;
}

void copyStaticContent(const char *themeDir, const char *inputDir, const char *outputDir) {
    char filePathA[256];
    char filePathB[256];

    // Setup output directory and copy necessary files.
    mkdir(outputDir, 0777);
    strcpy(filePathA, outputDir);
    strcat(filePathA, "/css");
    mkdir(filePathA, 0777);
    strcpy(filePathA, themeDir);
    strcat(filePathA, "/style.css");
    strcpy(filePathB, outputDir);
    strcat(filePathB, "/css/style.css");
    copyFile(filePathA, filePathB);
}

void loadTheme(const char *themePath) {
    char filePath[256];

    // Load root template.
    strcpy(filePath, themePath);
    strcat(filePath, "/templates/root.html");
    rootTemplate = TS_ParseFile(filePath);

    // Load nav template.
    strcpy(filePath, themePath);
    strcat(filePath, "/templates/nav.html");
    navTemplate = TS_ParseFile(filePath);

    // Load nav button template.
    strcpy(filePath, themePath);
    strcat(filePath, "/templates/nav_button.html");
    navButtonTemplate = TS_ParseFile(filePath);

    // Load nav button selected template.
    strcpy(filePath, themePath);
    strcat(filePath, "/templates/nav_button_selected.html");
    navButtonSelectedTemplate = TS_ParseFile(filePath);

    // Load indexer template.
    strcpy(filePath, themePath);
    strcat(filePath, "/templates/indexer.html");
    indexerTemplate = TS_ParseFile(filePath);

    // Load index card template.
    strcpy(filePath, themePath);
    strcat(filePath, "/templates/index_card.html");
    indexCardTemplate = TS_ParseFile(filePath);

    // Load article template.
    strcpy(filePath, themePath);
    strcat(filePath, "/templates/article.html");
    articleTemplate = TS_ParseFile(filePath);
}

void loadSiteConfig(const char *inputDir) {
    char filePath[256];

    strcpy(filePath, inputDir);
    strcat(filePath, "/site.conf");
    siteConfig = TS_ParseFile(filePath);
    pruneDirectiveList(siteConfig, "STRING");

    siteContexts = ripDirectiveList(siteConfig, "DEFCONTEXT");
}

void unloadTheme() {
    destroyDirectiveList(rootTemplate);
    destroyDirectiveList(navTemplate);
    destroyDirectiveList(navButtonTemplate);
    destroyDirectiveList(navButtonSelectedTemplate);
    destroyDirectiveList(indexerTemplate);
    destroyDirectiveList(indexCardTemplate);
    destroyDirectiveList(articleTemplate);
}

void unloadSiteConfig() {
    destroyDirectiveList(siteConfig);
    destroyDirectiveList(siteContexts);
}

void parseArgs(int argc, char const *argv[]) {
    for (int i = 1; i < argc; i++) {
        if (strcmp(argv[i], "--help") == 0) {
            puts(helpText);
            exit(0);
        }

        if (strcmp(argv[i], "--version") == 0) {
            puts(versionText);
            exit(0);
        }

        else if (strcmp(argv[i], "-o") == 0) {
            if (i + 1 == argc) {
                printf("%s: missing parameter for option '-o'\n", argv[0]);
                puts(helpText);
                exit(1);
            }

            i++;

            free(outputDir);
            outputDir = (char *)malloc(sizeof(char) * (strlen(argv[i]) + 1));
            strcpy(outputDir, argv[i]);
        }

        else if (strcmp(argv[i], "-t") == 0) {
            if (i + 1 == argc) {
                printf("%s: missing parameter for option '-t'\n", argv[0]);
                puts(helpText);
                exit(1);
            }

            i++;

            free(themeDir);
            themeDir = (char *)malloc(sizeof(char) * (strlen(argv[i]) + 1));
            strcpy(themeDir, argv[i]);
        }

        else {
            if (i + 1 != argc) {
                printf("%s: invalid option '%s'\n", argv[0], argv[i]);
                puts(helpText);
                exit(1);
            }

            free(inputDir);
            inputDir = (char *)malloc(sizeof(char) * (strlen(argv[i]) + 1));
            strcpy(inputDir, argv[i]);
        }
    }
}

void setDefaults() {
    themeDir = (char *)malloc(sizeof(char) * (strlen(defaultThemeDir) + 1));
    strcpy(themeDir, defaultThemeDir);
 
    inputDir = (char *)malloc(sizeof(char) * (strlen(defaultInputDir) + 1));
    strcpy(inputDir, defaultInputDir);
 
    outputDir = (char *)malloc(sizeof(char) * (strlen(defaultOutputDir) + 1));
    strcpy(outputDir, defaultOutputDir);
}

int main(int argc, char const *argv[]) {
    char filePath[256];
    TS_Directive *directive;
    int index = 0;

    setDefaults();
    parseArgs(argc, argv);

    loadTheme(themeDir);
    loadSiteConfig(inputDir);
    copyStaticContent(themeDir, inputDir, outputDir);

    LL_Seek(siteContexts, index);
    directive = (TS_Directive *)LL_Get(siteContexts);

    while (directive != NULL) {
        printf("-- Context: %s --\n", directive->args[0]);
        
        if (strcmp(directive->args[2], "static") == 0) {
            LinkedList *tempContent;
            LinkedList *tempPage;
            

            strcpy(filePath, inputDir);
            strcat(filePath, "/");
            strcat(filePath, directive->args[1]);
            printf(" > %s\n", filePath);
            tempContent = TS_ParseFile(filePath);

            tempPage = buildPage(tempContent, directive->args[0]);

            strcpy(filePath, outputDir);
            strcat(filePath, "/");
            strcat(filePath, directive->args[1]);
            writeOutput(filePath, tempPage);

            destroyDirectiveList(tempContent);
            destroyDirectiveList(tempPage);
        }

        else if (strcmp(directive->args[2], "enumerable") == 0) {
            char *contentFile;
            LinkedList *contentFileList;
            LinkedList *tempPage;
            LinkedList *tempArticle;
            LinkedList *tempIndexer;
            LinkedList *indexCardList = LL_New();

            strcpy(filePath, outputDir);
            strcat(filePath, "/");
            strcat(filePath, directive->args[1]);
            mkdir(filePath, 0777);

            strcpy(filePath, inputDir);
            strcat(filePath, "/");
            strcat(filePath, directive->args[1]);
            
            contentFileList = getFileNamesByExtension(filePath, ".html");

            // TODO: Build tag lists.
            
            contentFileList = buildArticleList(contentFileList, filePath);
            
            LL_Seek(contentFileList, 0);
            contentFile = LL_Get(contentFileList);

            char url[256] = {0};

            while (contentFile != NULL) {
                LinkedList *tempIndexCard;
                char prevURL[265];
                char *prevFileName;

                strcpy(filePath, inputDir);
                strcat(filePath, "/");
                strcat(filePath, directive->args[1]);
                strcat(filePath, "/");
                strcat(filePath, contentFile);
                printf(" > %s\n", filePath);

                if ((prevFileName = LL_Next(contentFileList)) != NULL) {
                    strcpy(prevURL, "/");
                    strcat(prevURL, directive->args[1]);
                    strcat(prevURL, "/");
                    strcat(prevURL, prevFileName);
                    LL_Prev(contentFileList);
                }

                else strcpy(prevURL, "\0");
                
                tempArticle = buildArticle(filePath, url, prevURL);
                tempPage = buildPage(tempArticle, directive->args[0]);
                
                strcpy(url, "/");
                strcat(url, directive->args[1]);
                strcat(url, "/");
                strcat(url, contentFile);

                tempIndexCard = buildIndexCard(filePath, url);
                concatenateDirectiveList(indexCardList, tempIndexCard);
                
                strcpy(filePath, outputDir);
                strcat(filePath, "/");
                strcat(filePath, directive->args[1]);
                strcat(filePath, "/");
                strcat(filePath, contentFile);
                writeOutput(filePath, tempPage);
                
                destroyDirectiveList(tempArticle);
                destroyDirectiveList(tempPage);
                destroyDirectiveList(tempIndexCard);
                
                contentFile = LL_Next(contentFileList);
            }

            tempIndexer = buildIndexer(indexCardList);
            tempPage = buildPage(tempIndexer, directive->args[0]);

            strcpy(filePath, outputDir);
            strcat(filePath, "/");
            strcat(filePath, directive->args[1]);
            strcat(filePath, "/");
            strcat(filePath, "index.html");
            writeOutput(filePath, tempPage);

            destroyDirectiveList(indexCardList);
            destroyDirectiveList(tempIndexer);
            destroyDirectiveList(tempPage);

            LL_Destroy(contentFileList, true);
        }

        LL_Seek(siteContexts, index);
        directive = (TS_Directive *)LL_Next(siteContexts);
        index++;
    }

    unloadTheme();
    unloadSiteConfig();
 
    return 0;
}

// Copyright © 2022 Jesse Smith (jesse@tinyplace.net) - All Rights Reserved.
