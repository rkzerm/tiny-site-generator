# Tiny Site Generator

### **Current Release: 0.0.2**
### Licensed under [GPLv3](https://gnu.org/licenses/gpl.html)
### *Copyright © 2022 Jesse Smith (jesse@tinyplace.net) - All Rights Reserved.*

The Tiny Site Generator or "tinysg" is a command line tool for static HTML site generation.  TSG makes maintaining small sites and blogs a breeze with even the most lean of development environments.  The purpose of TSG is to be light weight and portable without the need for frameworks, databases, package managers, scripting languages, or other complex tool chains.  Simply pure, elegant HTML.

TSG is free and open source.  It is written completely in C with no external dependencies beyond the C standard library making it highly portable.  TSG currently aims to support any Linux based environment linking with GNU or Musl LibC.  

## v0.1.0 Goals
* ~~Sorting enumerated content by publish date.~~
* ~~Next/Prev article navigation.~~
* Tag based article indexes.
* Basic SEO features.
* ~~Fix publish flag.~~
* Complete code documentation.

## Build and Install

1. Clone this repository and build the `tinysg` tool with `make`.
2. You can verify everything is working by running `make test`.  This will use the built `tinysg` to build the included test site into a `./dist` folder.  If you have Python3, you can run `python -m http.server 9000 --directory ./dist` to run a local test server.  Open a browser and navigate to `http://localhost:9000` to view the test site.
3. Next, you can install `tinysg` with `sudo make install`.  

Now you can run `tinysg` command from a terminal to build static HTML sites.

## How to use TSG

### Run `tinysg --help` for command line usage.

### Anatomy of a TSG site

```
my_blog_site/
├─ site.conf
├─ index.html
├─ about.html
└─ blog/
   ├─ some-article.html
   ├─ another-article.html
   └─ and-yet-another.html
```

A TSG site project contains at its root, a `site.conf` which contains directives that tell TSG how the site is structured and some global attributes of the site.  TSG looks at the `site.conf` first to tell it how to interpret the rest of the project files.  

TSG looks for directives within the project files marked by a special delimeter.  Here is a directive that sets the site title: `<!-- @SITETITLE:My Awesome Blog! -->`.  Various directives will be used in your project to tell TSG how to generated the page.  Directives are also found in the theme templates TSG uses to generate the finished HTML pages.

I recommend you copy the `test_site` directory and modify the example provided to get you started.  

## See Also

* Check out my [TinyPlace Blog](https://tinyplace.net/blog) which was generated using TSG.
* For more information about Tiny Site Generator, check out [this article](https://tinyplace.net/blog/tiny-site-generator.html).

### *Copyright © 2022 Jesse Smith (jesse@tinyplace.net) - All Rights Reserved.*
