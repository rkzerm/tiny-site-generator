/*******************************************************************************
* Tiny Site Generator - Generate static HTML pages.
* Copyright © 2022 Jesse Smith (jess@tinyplace.net) - All Rights Reserved.
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <https://www.gnu.org/licenses/>.
*******************************************************************************/

///////////////////////////////////////////////////////////////////////////////
// llist.h
///////////////////////////////////////////////////////////////////////////////
// See C file for complete description.
//
// Copyright © 2022 Jesse Smith (jesse@tinyplace.net) - All Rights Reserved.

///////////////////////////////////////////////////////////////////////////////
// System Libraries

#include <stdbool.h>

///////////////////////////////////////////////////////////////////////////////
// Defines

#define __LLIST__   // Include guard.

///////////////////////////////////////////////////////////////////////////////
// Type Definitions

// LinkedList Node - Represents a node in the list.

typedef struct LinkedListNode {
    void *item;                     // Pointer to an external item referenced by this node.
    struct LinkedListNode *next;    // Pointer to next node in the list.
    struct LinkedListNode *prev;    // Pointer to previous node in the list.
} LinkedListNode;

// LinkedList - Structure that defines the list and holds the read state.

typedef struct LinkedList {
    LinkedListNode *root;   // Pointer to first node of the list.
    LinkedListNode *ptr;    // Pointer to current node to operate on or read from.
    LinkedListNode *last;   // Pointer to last node in the list.
    int length;             // Length of the list.
} LinkedList;

///////////////////////////////////////////////////////////////////////////////
// Function Prototypes

LinkedList *LL_New();   // Instantiate a new list.
void LL_Append(LinkedList *list, void *item);   // Append item to list.
void *LL_Delete(LinkedList *list);  // Delete node from list.
void LL_Destroy(LinkedList *list, bool deep);   // Destroy a list.
void LL_Insert(LinkedList *list, void *item);   // Insert item into list.
void LL_Seek(LinkedList *list, int pos);    // Set the current node in the list.
void *LL_Next(LinkedList *list);    // Get the next node.
void *LL_Prev(LinkedList *list);    // Get the previous node.
void *LL_Get(LinkedList *list);     // Get the current node.
void LL_Set(LinkedList *list, void *item);  // Set the current node.
void LL_InsertList(LinkedList *list, LinkedList *fragment); // Insert a list into another list.

// Copyright © 2022 Jesse Smith (jesse@tinyplace.net) - All Rights Reserved.
