/*******************************************************************************
* Tiny Site Generator - Generate static HTML pages.
* Copyright © 2022 Jesse Smith (jess@tinyplace.net) - All Rights Reserved.
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <https://www.gnu.org/licenses/>.
*******************************************************************************/

// Copyright © 2022 Jesse Smith (jesse@tinyplace.net) - All Rights Reserved.

#include <stdbool.h>

#ifndef __LLIST__
    #include "./llist.h"
#endif

#define TS_NUM_DIRECTIVES       22
#define TS_DIRECTIVE_MAX_LENGTH 24

typedef enum TS_ParseNodeType {
    STRING,
    DIRECTIVE,
    PARSE_ERROR
} TS_ParseNodeType;

typedef struct TS_Directive {
    //TS_DirectiveType type;
    int type;
    int numArgs;
    char **args;
} TS_Directive;

typedef struct TS_ParseNode {
    TS_ParseNodeType type;
    void *data;
    struct TS_ParseNode *next;
    struct TS_ParseNode *prev;
} TS_ParseNode;

LinkedList *TS_ParseFile(char filePath[]);
TS_Directive *TS_ParseDirective(char *str);
TS_Directive *TS_NewDirective(char *dir, int numArgs, char **args);
void TS_DestroyDirective(TS_Directive *directive);
TS_Directive *TS_CloneDirective(TS_Directive *directive);
bool TS_DirectiveTypeIs(TS_Directive *directive, char *type);
char *TS_DirectiveGetType(TS_Directive *directive);

// Copyright © 2022 Jesse Smith (jesse@tinyplace.net) - All Rights Reserved.
