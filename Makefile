TARGET		?= tinysg
CC			?= gcc
CFLAGS		?= -std=gnu99 -Wall
LINK_FILES	?= ./tinysite.c ./utils.c ./llist.c
LIBS		?= -L /usr/lib -L/usr/local/lib
BINDIR		?= /usr/local/bin/$(TARGET)
THEMEDIR	?= /usr/local/share/$(TARGET)/themes/

all:
	mkdir -p ./bin
	$(CC) main.c $(LINK_FILES) -o ./bin/$(TARGET) $(CFLAGS) $(LIBS)

debug:
	mkdir -p ./debug
	$(CC) main.c $(LINK_FILES) -o ./debug/$(TARGET) $(CFLAGS) -g $(LIBS)
	gdb ./debug/$(TARGET)

install:
	mkdir -p $(THEMEDIR)
	cp -r ./themes/* $(THEMEDIR)
	cp ./bin/$(TARGET) $(BINDIR)

uninstall:
	rm $(BINDIR)
	rm -r $(THEMEDIR)

test:
	./bin/$(TARGET) -o ./dist -t ./themes/tsg-light ./test_site/

clean:
	rm -rf *.o ./bin ./dist ./debug

