/*******************************************************************************
* Tiny Site Generator - Generate static HTML pages.
* Copyright © 2022 Jesse Smith (jess@tinyplace.net) - All Rights Reserved.
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <https://www.gnu.org/licenses/>.
*******************************************************************************/

///////////////////////////////////////////////////////////////////////////////
//
///////////////////////////////////////////////////////////////////////////////
//

// Copyright © 2022 Jesse Smith (jesse@tinyplace.net) - All Rights Reserved.

///////////////////////////////////////////////////////////////////////////////
// System Headers

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <dirent.h>
#include <stdbool.h>

///////////////////////////////////////////////////////////////////////////////
//Local Headers

#include "./tinysite.h"
#include "./utils.h"
//#include "./llist.h"

///////////////////////////////////////////////////////////////////////////////
// Constants

const char startDelimeter[] = "<!-- @";
const char endDelimeter[] = " -->";
const char argSeperator[] = "|";


const char directiveLookup[TS_NUM_DIRECTIVES][TS_DIRECTIVE_MAX_LENGTH] = {
    "STRING",
    "PARSEERROR",
    "PAGETITLE",
    "SITETITLE",
    "COPYRIGHT",
    "NAV",
    "CONTENT",
    "NAVBUTTONS",
    "NAVLABEL",
    "NAVURL",
    "DEFCONTEXT",
    "INDEXER",
    "ARTICLEURL",
    "ARTICLETITLE",
    "ARTICLEPUBLISH",
    "ARTICLEPUBLISHDATE",
    "ARTICLEAUTHOR",
    "ARTICLETAGS",
    "ARTICLESUMMARY",
    "ARTICLECONTENT",
    "ARTICLEPREVURL",
    "ARTICLENEXTURL"
};

int _getDirectiveID(char *dir) {
    int id = -1;
    int i;

    for (i = 0; i < TS_NUM_DIRECTIVES; i++) {
        if (strcmp(dir, directiveLookup[i]) == 0) {
            id = i;
            break;
        }
    }

    return id;
}

TS_Directive *TS_NewDirective(char *dir, int numArgs, char **args) {
    int type;
    TS_Directive *directive = NULL;
    
    if ((type = _getDirectiveID(dir)) != -1) {
        directive = (TS_Directive *)malloc(sizeof(TS_Directive));

        directive->type = type;
        directive->numArgs = numArgs;
        directive->args = args;
    }

    return directive;
}

TS_Directive *TS_ParseDirective(char *str) {
    int i;
    char *buffer = (char *)malloc(sizeof(char) * (strlen(str) + 1));
    
    TS_Directive *directive = NULL;

    for (i = 0; i < strlen(str); i++) {
        if (str[i] == ':') break;
        buffer[i] = str[i];
    }

    buffer[i] = '\0';
    
    for (i = 0; i < TS_NUM_DIRECTIVES; i++) {
        if (strcmp(buffer, directiveLookup[i]) == 0) {
            int typeLength = strlen(directiveLookup[i]);

            directive = (TS_Directive *)malloc(sizeof(TS_Directive));
            directive->numArgs = 0;
            directive->args = NULL;
            directive->type = i;
            
            if (str[typeLength] == ':') {
                LinkedList *terms = splitstr(str + typeLength + 1, argSeperator);
                
                directive->numArgs = terms->length;
                directive->args = (char **)malloc(sizeof(char *) * directive->numArgs);

                for (i = 0; i < directive->numArgs; i++) {
                    directive->args[i] = (char *)LL_Get(terms);
                    LL_Next(terms);
                }

                LL_Destroy(terms, false); 
            }

            break;
        }
    }

    free(buffer);

    return directive;
}

LinkedList *TS_ParseFile(char filePath[]) {
    FILE *fptr; 
    int index = 0;
    int offset = 0;
    int a = 0;
    int b = 0;
    int i = 0;
    char *buffer;

    LinkedList *parseList = LL_New();
    TS_Directive *directive = NULL;

    fptr = fopen(filePath, "r");

    //puts("mark1");

    while((offset = fstrstr(fptr, startDelimeter)) != -1) {
        if (offset > 0) {
            //puts("mark1.1");
            char **args = (char **)malloc(sizeof(char *));
            args[0] = (char *)malloc(sizeof(char) * (offset + 1));
            //puts("mark1.2");

            fseek(fptr, index, SEEK_SET);
            for (i = 0; i < offset; i++) {
                args[0][i] = fgetc(fptr);
            }
            //puts("mark1.3");
            args[0][i] = '\0';
            //puts("mark1.4");
            directive = TS_NewDirective("STRING", 1, args);
            //puts("mark1.5");
            LL_Append(parseList, directive);
            //puts("mark1.6");
        }
        //puts("mark2");
        // Extract the directive.
        a = index + offset + strlen(startDelimeter);    // Index the end of the starting delimeter.
        fseek(fptr, a, SEEK_SET);

        if ((offset = fstrstr(fptr, endDelimeter)) == -1) break;    // If the directive has no close, break.
        b = a + offset; // Index the beginning of the ending delimeter.
        fseek(fptr, a, SEEK_SET);   // Set the read pointer to the end of the starting delimeter.

        buffer = (char *)malloc(sizeof(char) * ((b - a) + 1));  // Allocate a buffer for the directive string between the start and end delimeter.
        for (i = 0; i < b - a; i++) {   // Read the directive into the buffer.
            buffer[i] = fgetc(fptr);
        }

        buffer[i] = '\0';   // Null terminate the directive string.
        directive = TS_ParseDirective(buffer);

        //puts("mark3");
        if (directive == NULL) {
            char **args = (char **)malloc(sizeof(char *));
            args[0] = buffer;
            directive = TS_NewDirective("PARSEERROR", 1, args);
            LL_Append(parseList, directive);
        }

        else {
            LL_Append(parseList, directive);
            free(buffer);
        }

        index = b + strlen(endDelimeter);   // Advance the read index to the end of the end delimeter.
        fseek(fptr, index, SEEK_SET);   // Set the read pointer to the read index to begin looking for the next directive.
        //puts("mark4");
    }
    //puts("mark5");
    fseek(fptr, index, SEEK_SET);
    offset = 0;
    while (fgetc(fptr) != EOF) offset++;

    if (offset > 0) {
        fseek(fptr, index, SEEK_SET);

        char **args = (char **)malloc(sizeof(char *));
        args[0] = (char *)malloc(sizeof(char) * (offset + 1));

        for (i = 0; i < offset; i++) {
            args[0][i] = fgetc(fptr);
        }

        args[0][i] = '\0';
        
        directive = TS_NewDirective("STRING", 1, args);
        LL_Append(parseList, directive);
    }
    //puts("mark6");

    fclose(fptr);   // Close the article file.

    return parseList;
}

void TS_DestroyDirective(TS_Directive *directive) {
    int i;

    for (i = 0; i < directive->numArgs; i++) {
        free(directive->args[i]);
    }

    free(directive->args);
    free(directive);
}

TS_Directive *TS_CloneDirective(TS_Directive *directive) {
    int i;
    TS_Directive *clone = (TS_Directive *)malloc(sizeof(TS_Directive));

    clone->type = directive->type;
    clone->numArgs = directive->numArgs;
    clone->args = (char **)malloc(sizeof(char *) * directive->numArgs);

    for (i = 0; i < directive->numArgs; i++) {
        clone->args[i] = (char *)malloc(sizeof(char) * (strlen(directive->args[i]) + 1));
        strcpy(clone->args[i], directive->args[i]);
    }

    return clone;
}

bool TS_DirectiveTypeIs(TS_Directive *directive, char *type) {
    return (strcmp(directiveLookup[directive->type], type) == 0)? true : false;
}

char *TS_DirectiveGetType(TS_Directive *directive) {
    return directiveLookup[directive->type];
}

// Copyright © 2022 Jesse Smith (jesse@tinyplace.net) - All Rights Reserved.
