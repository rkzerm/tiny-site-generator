/*******************************************************************************
* Tiny Site Generator - Generate static HTML pages.
* Copyright © 2022 Jesse Smith (jess@tinyplace.net) - All Rights Reserved.
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <https://www.gnu.org/licenses/>.
*******************************************************************************/

///////////////////////////////////////////////////////////////////////////////
// utils.h
///////////////////////////////////////////////////////////////////////////////
// See C file for complete description.
//
// Copyright © 2022 Jesse Smith (jesse@tinyplace.net) - All Rights Reserved.

///////////////////////////////////////////////////////////////////////////////
// System Libraries

#include <stdbool.h>

///////////////////////////////////////////////////////////////////////////////
// Local Libraries

#ifndef __LLIST__
    #include "./llist.h"
#endif

///////////////////////////////////////////////////////////////////////////////
// Function Prototypes.

int copyFile(const char srcPath[], const char dstPath[]);   // Copy a binary file.
int fstrstr(FILE *fptr, const char *str);   // Search a file stream for a string.
bool startswith(const char *str, const char *sub);  // Determine if a string starts with a substring.
bool endswith(const char *str, const char *sub);    // Determine if a string ends with a substring.
LinkedList *splitstr(const char *str, const char *del); // Split a string into segments.
LinkedList *getFileNamesByExtension(const char *dirPath, const char *ext);  // List files in directory by extension.

// Copyright © 2022 Jesse Smith (jesse@tinyplace.net) - All Rights Reserved.
